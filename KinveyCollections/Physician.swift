//
//  Physicians.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/16/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import Foundation
import Kinvey
import ObjectMapper

//Kinvey collection object setup for Test example object
class Physician: Entity {
    
    //Add for each property
    @objc dynamic var _id: String?
    @objc dynamic var CustomerId: String?
    @objc dynamic var FirstName: String?
    @objc dynamic var LastName: String?
    @objc dynamic var Enabled: Int = 0
    
    
    override class func collectionName() -> String {
        //return the name of the backend collection corresponding to this entity
        return "Physician"
    }
    
    //Map properties in your backend collection to the members of this entity
    override func propertyMapping(_ map: Map) {
        
        //This maps the "_id", "_kmd" and "_acl" properties
        super.propertyMapping(map)
        
        //Each property in your entity should be mapped using the following scheme:
        //<member variable> <- ("<backend property>", map["<backend property>"])
        _id <- ("_id", map["_id"])
        CustomerId <- ("CustomerId", map["CustomerId"])
        FirstName <- ("FirstName", map["FirstName"])
        LastName <- ("LastName", map["LastName"])
        Enabled <- ("Enabled", map["Enabled"])
        
    }
}

