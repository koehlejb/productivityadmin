//
//  AspNetUser.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/10/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import Foundation
import Kinvey
import ObjectMapper

//Kinvey collection object setup for Test example object
class AspNetUsers: Entity {
    
    //Add for each property
    @objc dynamic var _id: String?
    @objc dynamic var CustomerNumber: String?
    @objc dynamic var ContactName: String?
    @objc dynamic var FacilityName: String?
    @objc dynamic var UserName: String?
    @objc dynamic var PhoneNumber: String?
    @objc dynamic var Email: String?
    @objc dynamic var RepEmail: String?
    @objc dynamic var RoleName: String?
    //@objc dynamic var Id: String?
    @objc dynamic var DeviceId: String?
    @objc dynamic var Enabled: Int32 = 1
    @objc dynamic var EmailConfirmed: Int32 = 0
    @objc dynamic var PhoneNumberConfirmed: Int32 = 0
    @objc dynamic var TwoFactorEnabled: Int32 = 0
    @objc dynamic var LockoutEndDateUtc: String?
    @objc dynamic var LockoutEnabled: Int32 = 0
    @objc dynamic var AccessFailedCount: Int32 = 0
    @objc dynamic var DefaultBillToAddressId: Int32 = 0
    @objc dynamic var DefaultShipToAddressId: Int32 = 0
    
    
    
    override class func collectionName() -> String {
        //return the name of the backend collection corresponding to this entity
        return "AspNetUsers"
    }
    
    //Map properties in your backend collection to the members of this entity
    override func propertyMapping(_ map: Map) {
        
        //This maps the "_id", "_kmd" and "_acl" properties
        super.propertyMapping(map)
        
        //Each property in your entity should be mapped using the following scheme:
        //<member variable> <- ("<backend property>", map["<backend property>"])
        CustomerNumber <- ("CustomerNumber", map["CustomerNumber"])
        ContactName <- ("ContactName", map["ContactName"])
        FacilityName <- ("FacilityName", map["FacilityName"])
        UserName <- ("UserName", map["UserName"])
        PhoneNumber <- ("PhoneNumber", map["PhoneNumber"])
        Email <- ("Email", map["Email"])
        RepEmail <- ("RepEmail", map["RepEmail"])
        RoleName <- ("RoleName", map["RoleName"])
        _id <- ("_id", map["_id"])
        DeviceId <- ("DeviceId", map["DeviceId"])
        Enabled <- ("Enabled", map["Enabled"])
        EmailConfirmed <- ("EmailConfirmed", map["EmailConfirmed"])
        PhoneNumberConfirmed <- ("PhoneNumberConfirmed", map["PhoneNumberConfirmed"])
        TwoFactorEnabled <- ("TwoFactorEnabled", map["TwoFactorEnabled"])
        LockoutEndDateUtc <- ("LockoutEndDateUtc", map["LockoutEndDateUtc"])
        LockoutEnabled <- ("LockoutEnabled", map["LockoutEnabled"])
        AccessFailedCount <- ("AccessFailedCount", map["AccessFailedCount"])
        DefaultBillToAddressId <- ("DefaultBillToAddressId", map["DefaultBillToAddressId"])
        DefaultShipToAddressId <- ("DefaultShipToAddressId", map["DefaultShipToAddressId"])
        
        
    }
}
