//
//  UserListingTVCell.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/11/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit

class UserListingTVCell: UITableViewCell {
    @IBOutlet weak var txbContactName: UITextField!
    @IBOutlet weak var txbFacilityName: UITextField!
    @IBOutlet weak var txbCustomerNumber: UITextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
