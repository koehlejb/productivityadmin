//
//  PhysicianTVCell.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/16/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit

class PhysicianTVCell: UITableViewCell {
    
    @IBOutlet weak var txbPhysician: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
