//
//  EditPhysicianTVCell.swift
//  ProductivityAdmin
//
//  Created by localadmin on 2/2/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit

class EditPhysicianTVCell: UITableViewCell {

    @IBOutlet weak var swEnabled: UISwitch!
    @IBOutlet weak var txbFirstname: UITextField!
    @IBOutlet weak var txbLastname: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
