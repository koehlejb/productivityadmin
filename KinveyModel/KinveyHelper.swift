//
//  KinveyHelper.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/10/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class KinveyHelper: NSObject {
    
//Make this a shared instance or singleton
static let sharedInstance = KinveyHelper()
    
//Global response wait
var blnGotResponse: Bool = false
    
//Need a default initializer
override init() {
    super.init()
    
    print("Kinvey Helper Initialized")
    
}
   
//Var to hold return from find in Kinvey - temp
var users = AnyRandomAccessCollection<AspNetUsers>([])

//Assume failure to create shared client
var blnKinveySharedClientOK = false
    
//Connection verified?
var blnKinveyConnectionVerified = false
    
//User is logged in OK
var blnUserLoggedInOK = false
    
//If shared client is created ok this gets called
public func setKinveySharedClientStatusOK() {
    blnKinveySharedClientOK = true
}
    
//If the Kinvey connect verified successfully then this is set also
public func setKinveyConnectionVerified() {
    blnKinveyConnectionVerified = true
}

//If the Kinvey connect verified successfully then this is set also
func setUserLoggedInOk() {
    blnUserLoggedInOK = true
}

//Get the Kinvey login status
func getLoginStatus() -> Bool {
   return blnUserLoggedInOK
}
    
//Func to check on response complete
public func getResponseStatus() -> Bool {
  return blnGotResponse
}
    
public func getAllUsers() -> AnyRandomAccessCollection<AspNetUsers> {
    
//    //Var to track response
//    self.blnGotResponse = false
//
//    //Create the Datastore for the AspNetUsers collection
//    let dataStore = DataStore<AspNetUsers>.collection()
//
//    //Do a find on the collection and print all rows
//    dataStore.find() { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
//        switch result {
//        case .success(let AspNetUsers):
//            print("AspNetUsers: \(AspNetUsers)")
//            self.users = AspNetUsers
//            print(self.users.count)
//
//            //For now just display the data
//            if self.users.count>0 {
//                for index in 0...self.users.count-1 {
//                    print(self.users[index].ContactName as String!)
//                    print(self.users[index].CustomerNumber as String!)
//                    print(self.users[index].FacilityName as String!)
//                    print("-----------------------------------------")
//                    print("")
//                }
//            }
//
//          //Update reponse
//          self.blnGotResponse = true
//        case .failure(let error):
//            print("Error: \(error)")
//
//            //Update reponse
//            self.blnGotResponse = true
//        }
//    }
//
    return self.users
}
    
//This method required before you can access Kinvey data
public func loginKinveyUser(username: String, password: String) {
    
    //Var to track response
    self.blnGotResponse = false

    //Now login to Kinvey instance you need at least one user to get to the data could be generic user also look into MIC for using AD
    User.login(username: username, password: password, options: nil) { (result: Result<User, Swift.Error>) in
        switch result {
        case .success(let user):
            //the log-in was successful and the user is now the active user and credentials saved
            print("User: \(user)")
            print(user.username as String!)
            
            //Store login OK
            self.setUserLoggedInOk()
            
            //Update reponse
            self.blnGotResponse = true
            
        case .failure(let error):
            //there was an error with the update save
            let message = error.localizedDescription
            print(message)
            
            //Update reponse
            self.blnGotResponse = true
        }
        
    }
    
}
    
}
