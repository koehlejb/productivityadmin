//
//  SharedMethods.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/12/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit

class SharingManager: NSObject {
    
    //Make this a shared instance or singleton
    static let sharedInstance = SharingManager()
    
    //Properties
    var customerID: String = ""
    
    //Need a default initializer
    override init() {
        super.init()
        
        print("Sharing manager Initialized")
    }
    
    //Take a key and value and save as NSUserDefault plus synchronize
    public func setUserDefaultForKeyAndValue (key: String, value: String) {
        
        //Setup user defaults
        let defaults = UserDefaults.standard
        //Set the value for key provided
        defaults.setValue(value, forKey: key)
        //Synchonize the change
        defaults.synchronize()
        
    }
    
    //Store selected customer ID
    public func setCustomerId (custID: String) {
        customerID = custID
    }
    
    //Return selected customer ID
    public func getCustomerId () -> String {
        return customerID
    }
    
   
    
}
