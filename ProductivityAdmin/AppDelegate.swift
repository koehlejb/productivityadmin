//
//  AppDelegate.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/9/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let kinveyHelper = KinveyHelper.sharedInstance
    
    //Var to hold return from find in Kinvey
    //var users = AnyRandomAccessCollection<AspNetUsers>([])

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
       //Setup the shared client for Kinvey
        Kinvey.sharedClient.initialize(
            //appKey: "kid_HkKtSUabz", - Development
            //appSecret: "cf782759582c449eb9985c8a593f3027" - Development Env.,
            appKey: "kid_Hyn1iqLbm",
            appSecret: "62edbcd749e34894bb379500b89af643",
            apiHostName: URL(string: "https://bscit-us1-baas.kinvey.com")!
            
        ) {
            switch $0 {
            case .success(let user):
                if let user = user {
                    print("User: \(user)")
                    
                    //Turn on the logging
                    Kinvey.logLevel = .debug
                    Kinvey.sharedClient.logNetworkEnabled = true
                    
                    //Record status good for client
                    self.kinveyHelper.setKinveySharedClientStatusOK()
                }
            case .failure(let error):
                print("Error: \(error)")
                
            }
        }
        
        //This is to verify the connection
        Kinvey.sharedClient.ping() { (result: Result<EnvironmentInfo, Swift.Error>) in
            switch result {
            case .success(let envInfo):
                print(envInfo)
                self.kinveyHelper.setKinveyConnectionVerified()
            case .failure(let error):
                print(error)
            }
        }
        
        IQKeyboardManager.sharedManager().enable = true
       
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

