//
//  LoginViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/10/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class LoginViewController: UIViewController,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
    
    
    @IBOutlet weak var pkrEnvironment: UIPickerView!
    @IBOutlet weak var lblStatusMessage: UILabel!
    @IBOutlet weak var tbxUsername: UITextField!
    @IBOutlet weak var tbxPassword: UITextField!
    @IBOutlet weak var swtSave: UISwitch!
    
    //Create the shared methods object singleton
    let sm = SharingManager.sharedInstance
    
    //Function to show alerts to user
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        
        //Validation Checks
        if self.tbxUsername.text == "" {
            self.lblStatusMessage.text = "Please enter your username ..."
            return
        }
        
        if self.tbxPassword.text == "" {
            self.lblStatusMessage.text = "Please enter your password ..."
            return
        }
        
        if self.swtSave.isOn {
            //save the username
            sm.setUserDefaultForKeyAndValue(key: "username", value: self.tbxUsername.text!)
        }
        
        //Tell user login in progress, could add activity indicator.
        self.lblStatusMessage.text = "Logging in one moment ..."
        
        //Login the user
        User.login(username: self.tbxUsername.text!, password: self.tbxPassword.text!, options: nil) { (result: Result<User, Swift.Error>) in
            switch result {
            case .success(let user):
                //the log-in was successful and the user is now the active user and credentials saved
                print("User: \(user)")
                print(user.username as String!)
                
                self.performSegue(withIdentifier: "ShowUserListing", sender: self)
                
            case .failure(let error):
                //there was an error with the update save
                let message = error.localizedDescription
                print(message)
                
                //login failed
                self.lblStatusMessage.text = "The login failed for user ..."
                self.showAlert(title: "Productivity Admin - Error", message: message)
                //Try again
                self.lblStatusMessage.text = "Please login ..."
            }
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.lblStatusMessage.text = "Please login ..."
        
        //User the defaults to see if they user has logged in before
        let defaults = UserDefaults.standard
        
        if let username = defaults.value(forKey: "username") {
           self.tbxUsername.text = username as? String
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Add fields to resign first responder here
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard
        self.view.endEditing(true)
    }
    
    //Picker view components
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    //Pickerview rows
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 2
    }
    
    //Return the options there are only two.
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        let myfont = UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)
        label?.font = myfont
        
        if row == 0
        {
            label?.text = "Stage"
        } else {
            label?.text = "Production"
        }
        
        label?.textAlignment = .center
        return label!
    }

}
