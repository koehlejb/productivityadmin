//
//  AddUserViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/17/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class AddUserViewController: UIViewController {
    
    //Create the Datastore for the AspNetUsers collection
    // Get an instance
    let dataStore = DataStore<AspNetUsers>.collection()
    
    //Add the sharing manager
    let sm = SharingManager.sharedInstance
    
    //Var to hold return from find for users
    var user = AnyRandomAccessCollection<AspNetUsers>([])
    
    //Create object for update user
    var updateUser = AspNetUsers()
    
    @IBAction func btnClearID(_ sender: UIButton) {
        //Clear the deviceID so user can reinstall
        
        let alert = UIAlertController(title: "Verify Clear Device ID", message: "Are you sure you want to clear the device ID for this user?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            var selectedID: String = ""
            selectedID = self.sm.getCustomerId()
            
            //Show selected ID
            print("SelectedID = \(selectedID)")
            
            //Query for all fields using Id.
            let query = Query(format: "_id == %@", selectedID )
            //Find then update then save.
            self.dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                switch result {
                case .success(let AspNetUsers):
                    print("AspNetUser: \(AspNetUsers)")
                    self.updateUser = AspNetUsers[0]
                    print("Before Update:")
                    print(self.updateUser)
                    //self.updateUser._id = self.updateUser.entityId!
                    self.updateUser.DeviceId = nil
                    print("After Update:")
                    print(self.updateUser)
                    
                    self.dataStore.save(self.updateUser, options: nil) { (result: Result<AspNetUsers, Swift.Error>) in
                        switch result {
                        case .success(let user):
                            print("User: \(user)")
                            self.showAlert(title: "User Clear Device ID", message: "The device ID for this user has been cleared ...")
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
            
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        //Show the alert
        self.present(alert, animated: true)
    }
    
    @IBOutlet weak var lblStatusMessage: UILabel!
    @IBOutlet weak var txbNewID: UITextField!
    @IBOutlet weak var txbCustNumber: UITextField!
    @IBOutlet weak var txbContactName: UITextField!
    @IBOutlet weak var txbFacilityName: UITextField!
    @IBOutlet weak var txbUserName: UITextField!
    @IBOutlet weak var txbPhoneNumber: UITextField!
    @IBOutlet weak var txbContactEmail: UITextField!
    @IBOutlet weak var txbRepEmail: UITextField!
    @IBOutlet weak var btnAddUser: UIButton!
    @IBOutlet weak var btnAddresses: UIButton!
    @IBOutlet weak var btnPhysicians: UIButton!
    @IBOutlet weak var btnClearID: UIButton!
    
    @IBOutlet weak var vwNewUser: UIView!
    @IBAction func btnResetAddUser(_ sender: UIBarButtonItem) {
        
        //Tell user to enter needed details
        self.lblStatusMessage.text = "Enter user details below, then press Add New ..."
        
        self.txbNewID.text = ""
        self.txbCustNumber.text = ""
        self.txbContactName.text = ""
        self.txbFacilityName.text = ""
        self.txbUserName.text = ""
        self.txbPhoneNumber.text = ""
        self.txbContactEmail.text = ""
        self.txbRepEmail.text = ""
        self.btnAddUser.isEnabled = true
        self.btnAddresses.isEnabled = false
        self.btnPhysicians.isEnabled = false
        
        self.btnAddUser.setTitle("Add New", for: UIControlState.normal)
        
        self.txbCustNumber.becomeFirstResponder()
        
    }
    
    @IBAction func bbiBack(_ sender: UIBarButtonItem) {
        sm.setCustomerId(custID: "")
        self.performSegue(withIdentifier: "ShowListingFromAddUser", sender: self)
    }
    
    
    @IBAction func btnAddUser(_ sender: UIButton) {
        print("Add User")
        
        //Check for all values supplied
        if self.txbCustNumber.text == "" {
            showAlert(title: "Add User Check", message: "Please enter a customer number.")
            self.txbCustNumber.becomeFirstResponder()
            return
        }
        if self.txbContactName.text == "" {
            showAlert(title: "Data Check", message: "Please enter a contact name.")
            self.txbContactName.becomeFirstResponder()
            return
        }
        if self.txbFacilityName.text == "" {
            showAlert(title: "Data Check", message: "Please enter a facility name.")
            self.txbFacilityName.becomeFirstResponder()
            return
        }
        if self.txbUserName.text == "" {
            showAlert(title: "Data Check", message: "Please enter a username.")
            self.txbUserName.becomeFirstResponder()
            return
        }
        if self.txbPhoneNumber.text == "" {
            showAlert(title: "Data Check", message: "Please enter a contact phone number.")
            self.txbPhoneNumber.becomeFirstResponder()
            return
        }
        if self.txbContactEmail.text == "" {
            showAlert(title: "Data Check", message: "Please enter a contact email.")
            self.txbContactEmail.becomeFirstResponder()
            return
        }
        if self.txbRepEmail.text == "" {
            showAlert(title: "Data Check", message: "Please enter a sales rep email.")
            self.txbRepEmail.becomeFirstResponder()
            return
        }
        
        //Check to see if phone number is in the right format first, if not then update it
        let phoneString = self.txbPhoneNumber.text!
        var formattedPhoneNumber: String = ""
        
        //Check if phone number is in correct format
        if phoneString.count == 12 && phoneString.contains(find: "-") {
            print("Phone number format is good, skip formatting.")
            formattedPhoneNumber = phoneString
        } else {
            //do phone number format checks
            
            //check for phone number length, only all ten digits
            if phoneString.count>10 {
                showAlert(title: "Data Check", message: "Phone number must be 10 digits only.")
                self.txbPhoneNumber.becomeFirstResponder()
                return
            }
            
            //Check for characters in string
            let num = Int(phoneString)
            if num == nil {
                showAlert(title: "Data Check", message: "Phone number must be numbers only.")
                self.txbPhoneNumber.becomeFirstResponder()
                return
            }
            
            //Now format phone number to 999-999-9999
            let index = phoneString.index(phoneString.startIndex, offsetBy: 3)
            let areaCodeString = phoneString.prefix(upTo: index)
            
            //Part one
            let start = phoneString.index(phoneString.startIndex, offsetBy: 3)
            let end = phoneString.index(phoneString.endIndex, offsetBy: -4)
            let result = phoneString[start..<end]
            let partOne = String(result)
            
            //Part two
            let indexEnd = phoneString.index(phoneString.endIndex, offsetBy: -4)
            let partTwo = phoneString[indexEnd...]
            
            //Now put it together
            formattedPhoneNumber = areaCodeString + "-" + partOne + "-" + partTwo
        }
        
        // Save an entity locally on the device. This will add the item to the
        // sync table to be pushed to your backend at a later time.
        // for new entity you use the AspNetUsers type but for update
        // you first find the object in the datastore, modify what you want to change
        // and then save it. This way the entity has an _id value and upsert applies.
        // If you don't do this you always get a new record.
        //Do this if not an update - newID is blank.
        
        if self.txbNewID.text == "" {
            
            let newUser = AspNetUsers()
            //Assign the values to the enitity
            
            //Get an unique ID value for the insert
            let uuid = NSUUID().uuidString.lowercased()
            
            //Assign a unique id to the id value
            self.txbNewID.text = uuid
            
            newUser._id = self.txbNewID.text!
            newUser.CustomerNumber = self.txbCustNumber.text!
            newUser.ContactName = self.txbContactName.text!
            newUser.FacilityName = self.txbFacilityName.text!
            newUser.UserName = self.txbUserName.text!
            newUser.PhoneNumber = formattedPhoneNumber
            newUser.Email = self.txbContactEmail.text!
            newUser.RepEmail = self.txbRepEmail.text!
            newUser.LockoutEndDateUtc = ""
            
            //Get the iphone guid to store with user.
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            print(deviceID)
            newUser.DeviceId = deviceID
            
            dataStore.save(newUser, options: nil) { (result: Result<AspNetUsers, Swift.Error>) in
                switch result {
                case .success(let user):
                    print("user saved: \(user)")
                    
                    if self.btnAddUser.currentTitle == "Add New" {
                        //Was and add new
                        self.showAlert(title: "Add New", message: "New user created and saved ...")
                        //Enable the other data type changes for this user.
                        self.btnAddresses.isEnabled = true
                        self.btnPhysicians.isEnabled = true
                        self.btnAddUser.isEnabled = false
                    } else {
                        //was and update
                        self.showAlert(title: "Update", message: "User data was updated ...")
                        //Enable the other data type changes for this user.
                        self.btnAddresses.isEnabled = true
                        self.btnPhysicians.isEnabled = true
                        self.btnAddUser.isEnabled = true
                    }
                    
                case .failure(let error):
                    print("Error: \(error)")
                    self.showAlert(title: "Add User", message: "Error adding user ...")
                }
            }
        } else {
            //this is an update
            //Find the entity to update first
            //Get the selected customerID
            var selectedID: String = ""
            selectedID = sm.getCustomerId()
            
            //Show selected ID
            print("SelectedID = \(selectedID)")
            
            //Query for all fields using Id.
            let query = Query(format: "_id == %@", selectedID )
            //Find then update then save.
            dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                switch result {
                case .success(let AspNetUsers):
                    print("AspNetUser: \(AspNetUsers)")
                    self.updateUser = AspNetUsers[0]
                    print("Before Update:")
                    print(self.updateUser)
                    //self.updateUser._id = self.updateUser.entityId!
                    self.updateUser.CustomerNumber = self.txbCustNumber.text!
                    self.updateUser.ContactName = self.txbContactName.text!
                    self.updateUser.FacilityName = self.txbFacilityName.text!
                    self.updateUser.UserName = self.txbUserName.text!
                    self.updateUser.PhoneNumber = formattedPhoneNumber
                    self.updateUser.Email = self.txbContactEmail.text!
                    self.updateUser.RepEmail = self.txbRepEmail.text!
                    print("After Update:")
                    print(self.updateUser)
                    
                    self.dataStore.save(self.updateUser, options: nil) { (result: Result<AspNetUsers, Swift.Error>) in
                        switch result {
                        case .success(let user):
                            print("User: \(user)")
                            self.showAlert(title: "User Update", message: "Your updates have been successfully saved ...")
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
            
        }
        
        // Sync local data with the backend
        // This will push data that is saved locally on the device to your backend;
        // and then pull any new data on the backend and save it locally.
//        dataStore.sync() { (result: Result<(UInt, AnyRandomAccessCollection<AspNetUsers>), [Swift.Error]>) in
//            switch result {
//            case .success(let count, let users):
//                print("\(count) Users: \(users)")
//            case .failure(let error):
//                print("Error: \(error)")
//            }
//        }
       
    }
    
    @IBAction func btnAddresses(_ sender: UIButton) {
        print("Add Address")
        
        //Show the add address view and hide the new user view
        self.performSegue(withIdentifier: "ShowAddressEdit", sender: self)
    }
    
    @IBAction func btnPhysicians(_ sender: UIButton) {
        print("Add Physician")
        
        //Show the add address view and hide the new user view
        self.performSegue(withIdentifier: "ShowPhysicianEdit", sender: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        // Do any additional setup after loading the view.
        //Tell user to enter needed details
        
        let selectedID: String?  = sm.getCustomerId()
        if selectedID != nil && selectedID != "" {
            self.txbNewID.text = selectedID
            //Query for all fields using Id.
            let query = Query(format: "_id == %@", selectedID! )
            dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                switch result {
                case .success(let AspNetUsers):
                    print("AspNetUser: \(AspNetUsers)")
                    self.user = AspNetUsers
                    self.txbCustNumber.text = self.user[0].CustomerNumber!
                    self.txbContactName.text = self.user[0].ContactName!
                    self.txbFacilityName.text = self.user[0].FacilityName!
                    self.txbUserName.text = self.user[0].UserName!
                    self.txbPhoneNumber.text = self.user[0].PhoneNumber!
                    self.txbContactEmail.text = self.user[0].Email!
                    self.txbRepEmail.text = self.user[0].RepEmail!
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
            self.lblStatusMessage.text = "Update user details below, then press update ..."
            self.btnAddUser.titleLabel?.text = "Update User"
            self.btnAddUser.setTitle("Update", for: UIControlState.normal)
            self.btnAddresses.isEnabled = true
            self.btnPhysicians.isEnabled = true
            self.btnClearID.isEnabled = true
        } else {
            self.lblStatusMessage.text = "Enter user details below, then press Add New ..."
            self.btnAddUser.setTitle("Add New", for: UIControlState.normal)
            self.btnAddresses.isEnabled = false
            self.btnPhysicians.isEnabled = false
            self.btnClearID.isEnabled = false
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Function to show alerts to user
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 

}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
