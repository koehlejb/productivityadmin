//
//  EditPhysicianViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 2/2/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class EditPhysicianViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    //Var to hold return from find for addresses
    var physicians = AnyRandomAccessCollection<Physician>([])
    
    let sm = SharingManager.sharedInstance
    
    //Global var for selected customerid
    var selectedID: String = ""
    
    //Create the Datastore for the addresses
    let physicianStore = DataStore<Physician>.collection()
    
    //Create the addressID var
    var SelectedPhysicianID: String = "0"
    
    //Object for doing an update
    var updatePhysician = Physician()
    
    
    //Function to show alerts to user
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(self.physicians.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let physicianCell = tvwPhysicians.dequeueReusableCell(withIdentifier: "EditPhysicianCell", for: indexPath) as! EditPhysicianTVCell
        
        physicianCell.txbLastname.text = self.physicians[indexPath.row].LastName!
        physicianCell.txbFirstname.text = self.physicians[indexPath.row].FirstName!
        
        if self.physicians[indexPath.row].Enabled == 1 {
            physicianCell.swEnabled.isOn = true
        } else {
            physicianCell.swEnabled.isOn = false
        }
        
        return physicianCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.SelectedPhysicianID = physicians[indexPath.row]._id!
        self.txbLastname.text = physicians[indexPath.row].LastName!
        self.txbFirstname.text = physicians[indexPath.row].FirstName!
    
        //Set the enabled to what it is for physician
        if physicians[indexPath.row].Enabled == 1 {
            self.swEnabled.isOn = true
        } else {
            self.swEnabled.isOn = false
        }
        
        self.btnUpdate.isEnabled = true
        self.btnDelete.isEnabled = true
        self.btnAdd.isEnabled = false
    }

    @IBOutlet weak var tvwPhysicians: UITableView!
    @IBOutlet weak var lblStatusMessage: UILabel!
    
    @IBAction func bbiNew(_ sender: UIBarButtonItem) {
        //New Physician
        
        self.txbLastname.text = ""
        self.txbFirstname.text = ""
        self.swEnabled.isOn = true
        self.txbFirstname.becomeFirstResponder()
        self.btnAdd.isEnabled = true
        self.btnUpdate.isEnabled = false
        self.btnDelete.isEnabled = false
    }
    
    @IBAction func bbiBack(_ sender: UIBarButtonItem) {
        //Go back to user
    }
    
    @IBOutlet weak var txbLastname: UITextField!
    @IBOutlet weak var txbFirstname: UITextField!
    @IBOutlet weak var swEnabled: UISwitch!
    @IBOutlet weak var btnAdd: UIButton!
    
    @IBAction func btnDeletePhysician(_ sender: UIButton) {
        //Delete physician
        
        let alert = UIAlertController(title: "Verify Physician Deletion", message: "Are you sure you want to delete this physician?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            //Check for value of SelectedPhysicianID
            if self.SelectedPhysicianID != "0" {
                //remove it.
                let query = Query(format: "_id == %@", String(self.SelectedPhysicianID))
                self.physicianStore.remove(query, options: nil) { (result: Result<Int, Swift.Error>) in
          
                    switch result {
                    case .success(let count):
                        print("Count: \(count)")
                        self.refreshPhysicianData()
                        self.tvwPhysicians.reloadData()
                        self.txbFirstname.text = ""
                        self.txbLastname.text = ""
                        self.swEnabled.isOn = true
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            } else {
                
                self.showAlert(title: "No Physician Selected", message: "Select physician to delete first ...")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        //Show the alert
        self.present(alert, animated: true)
        
    }
    
    @IBAction func btnUpdatePhysician(_ sender: UIButton) {
        //Update physician
        
        //Make sure a physician is selected for the update
        if SelectedPhysicianID == "0" {
            self.showAlert(title: "Update Physician", message: "Please select a physician first to update.")
            return
        }
        
        if self.txbFirstname.text! == "" {
            self.lblStatusMessage.text = "Please enter a firstname."
            self.txbFirstname.becomeFirstResponder()
            return
        }
        
        if self.txbLastname.text! == "" {
            self.lblStatusMessage.text = "Please enter a lastname."
            self.txbFirstname.becomeFirstResponder()
            return
        }
        
        var physicianEnabled: Int = 0
        if self.swEnabled.isOn {
            physicianEnabled = 1
        } else {
            physicianEnabled = 0
        }
        
        let query = Query(format: "_id = %@", String(self.SelectedPhysicianID))
        physicianStore.find(query) { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physician):
                
                //This gets the next ID to use.
                print("physician: \(physician)")
                self.updatePhysician = physician[0]
                
                //Update the values to save
                //self.updatePhysician._id = self.SelectedPhysicianID
                self.updatePhysician.FirstName = self.txbFirstname.text!
                self.updatePhysician.LastName = self.txbLastname.text!
                self.updatePhysician.Enabled = physicianEnabled
                
                print(self.updatePhysician)
                
                self.physicianStore.save(self.updatePhysician, options: nil) { (result: Result<Physician, Swift.Error>) in
                    switch result {
                    case .success(let physician):
                        print("Physician updated: \(physician)")
                        //Reset the address data to only this customers physicians
                        self.refreshPhysicianData()
                        self.tvwPhysicians.reloadData()
                    case .failure(let error):
                        print("Error: \(error.localizedDescription)")
                        self.showAlert(title: "Update Physician", message: "Error updating physician ...")
                    }
                }
                
            case .failure(let error):
                print("Error: \(error)")
            }
            
        }
    }
    
    func refreshPhysicianData() {
        //Get the selected customerID
        selectedID = sm.getCustomerId()
        
        //Show selected ID
        print("SelectedID = \(selectedID)")
        
        //Query for all addresses using Id.
        let queryPhysician = Query(format: "CustomerId == %@", selectedID )
        physicianStore.find(queryPhysician) { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physicians):
                print("Physicians: \(physicians)")
                self.physicians = physicians
                self.tvwPhysicians.reloadData()
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }
    
    @IBAction func btnAddPhysician(_ sender: UIButton) {
        //Add a new physician
        
        //Check for minimums
        
        if self.txbFirstname.text! == "" {
            self.lblStatusMessage.text = "Please enter a firstname."
            self.txbFirstname.becomeFirstResponder()
            return
        }
        
        if self.txbLastname.text! == "" {
            self.lblStatusMessage.text = "Please enter a lastname."
            self.txbFirstname.becomeFirstResponder()
            return
        }
        
        var physicianEnabled: Int = 0
        if self.swEnabled.isOn {
            physicianEnabled = 1
        } else {
            physicianEnabled = 0
        }
        
        //Var to hold the new physician.
        let newPhysician = Physician()
        
        //Var to hold next address ID value
        var nextPhysicianId: Int32 = 0

        let predicate = NSPredicate(format: "CustomerID == %@", self.sm.getCustomerId())
        let sortDescriptor = NSSortDescriptor(key: "_id", ascending: true)
        let querySort = Query(predicate: predicate, sortDescriptors: [sortDescriptor])
        physicianStore.find(querySort) { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physician):

                //This gets the next ID to use.
                print("physician: \(physician)")
                self.physicians = physician
                print(self.physicians.count)
                
                //print(Int32(self.physicians[self.physicians.count - 1].PhysicianId!)!)
                nextPhysicianId = Int32(Int32(self.physicians[self.physicians.count-1]._id!)! + 1)
                print(nextPhysicianId)
        
                //Update the values to save
                //newPhysician.PhysicianId = Int32(nextPhysicianId)
                //.PhysicianId = String(nextPhysicianId)
                newPhysician._id = String(nextPhysicianId)
                newPhysician.CustomerId = self.sm.getCustomerId()
                newPhysician.FirstName = self.txbFirstname.text!
                newPhysician.LastName = self.txbLastname.text!
                newPhysician.Enabled = physicianEnabled
                
                self.physicianStore.save(newPhysician, options: nil) { (result: Result<Physician, Swift.Error>) in
                    switch result {
                    case .success(let physician):
                        print("Physician saved: \(physician)")
                        //Reset the address data to only this customers physicians
                        self.refreshPhysicianData()
                        self.tvwPhysicians.reloadData()
                    case .failure(let error):
                        print("Error: \(error)")
                        self.showAlert(title: "Add Physician", message: "Error adding physician ...")
                    }
                }
        
                //Refresh the view to show newly added.
                self.refreshPhysicianData()
                self.tvwPhysicians.reloadData()
                
            case .failure(let error):
                print("Error: \(error)")
            }

        }

    }
        
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Get the selected customerID
        selectedID = sm.getCustomerId()
        
        //Show selected ID
        print("SelectedID = \(selectedID)")
        
        //Query for all addresses using Id.
        let queryPhysician = Query(format: "CustomerId == %@", selectedID )
        physicianStore.find(queryPhysician) { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physicians):
                print("Physicians: \(physicians)")
                self.physicians = physicians
                self.tvwPhysicians.reloadData()
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Add fields to resign first responder here
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard
        self.view.endEditing(true)
    }
}
