//
//  EditAddressViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/23/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class EditAddressViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UIPickerViewDelegate,UIPickerViewDataSource {
    
    //Var to hold return from find for users
    var user = AnyRandomAccessCollection<AspNetUsers>([])
    
    //Create the Datastore for the users
    let dataStore = DataStore<AspNetUsers>.collection()
    
    //Create the addressID var
    var addressID: String = ""
    
    //Create object for update user
    var updateUser = AspNetUsers()
    
    var updateAddress = SiteAddress()
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 55
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        let data = states[row]
        let title = NSAttributedString(string: data, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: UIFont.Weight.bold)])
        label?.attributedText = title
        label?.textAlignment = .center
        return label!
        
    }
    
    //Function to show alerts to user
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    let states = [ "AK",
                   "AL",
                   "AR",
                   "AS",
                   "AZ",
                   "CA",
                   "CO",
                   "CT",
                   "DC",
                   "DE",
                   "FL",
                   "GA",
                   "GU",
                   "HI",
                   "IA",
                   "ID",
                   "IL",
                   "IN",
                   "KS",
                   "KY",
                   "LA",
                   "MA",
                   "MD",
                   "ME",
                   "MI",
                   "MN",
                   "MO",
                   "MS",
                   "MT",
                   "NC",
                   "ND",
                   "NE",
                   "NH",
                   "NJ",
                   "NM",
                   "NV",
                   "NY",
                   "OH",
                   "OK",
                   "OR",
                   "PA",
                   "PR",
                   "RI",
                   "SC",
                   "SD",
                   "TN",
                   "TX",
                   "UT",
                   "VA",
                   "VI",
                   "VT",
                   "WA",
                   "WI",
                   "WV",
                   "WY"]
    
    //Var to hold return from find for addresses
    var addresses = AnyRandomAccessCollection<SiteAddress>([])
    
    //Add the sharing manager
    let sm = SharingManager.sharedInstance
    
    //Global var for selected customerid
    var selectedID: String = ""
    
    //Create the Datastore for the addresses
    let addressStore = DataStore<SiteAddress>.collection()
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(self.addresses.count)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let addressCell = tbvAddresses.dequeueReusableCell(withIdentifier: "AddressAddEditCell", for: indexPath) as! AddressEditTVCell
        
        addressCell.lblAddress.text = self.addresses[indexPath.row].Address!
        
        return addressCell
    }
    
    @IBOutlet weak var lblStatusMessage: UILabel!
    @IBOutlet weak var txbLine1: UITextField!
    @IBOutlet weak var txbLine2: UITextField!
    @IBOutlet weak var txbCity: UITextField!
    @IBOutlet weak var pkrState: UIPickerView!
    @IBOutlet weak var txbZip: UITextField!
    @IBOutlet weak var swDefaultBilling: UISwitch!
    @IBOutlet weak var swDefaultShipping: UISwitch!
    
    @IBOutlet weak var tbvAddresses: UITableView!
    
    @IBAction func tbiReset(_ sender: UIBarButtonItem) {
        //Reset the address data
        
        //Clear all values and set focus for entry
        self.txbLine1.text = ""
        self.txbLine2.text = ""
        self.txbCity.text = ""
        self.txbZip.text = ""
        self.pkrState.selectRow(0, inComponent: 0, animated: true)
        self.swDefaultShipping.isOn = false
        self.swDefaultBilling.isOn = false
        self.lblStatusMessage.text = "Enter your new address and then tap add ..."
        self.txbLine1.becomeFirstResponder()
        
        
    }
    
    @IBAction func btnUpdateAddress(_ sender: UIButton) {
        //Update the address
        
        if self.txbLine1.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include line 1. Please add line 1.")
            return
        }
        
        if self.txbCity.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include a city. Please add a city.")
            return
        }
        
        if self.txbZip.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include a zip code. Please add zip code.")
            return
        }
        
        if self.addressID != "0" {
            //update the address to the latest values
            
            //Query for all fields using Id.
            let query = Query(format: "_id == %@", self.addressID )
            //Find then update then save.
            addressStore.find(query) { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
                switch result {
                case .success(let address):
                    print("Address: \(address)")
                    self.updateAddress = address[0]
                    print("Before Update:")
                    print(self.updateAddress)
                   
                    //String for full address build.
                    var newAddressString: String = ""
                    
                    //build the new address string
                    //Check to see if line2 is provided add if it is
                    if self.txbLine2.text! != "" {
                        //line 2 has been provided so include it
                        newAddressString = self.txbLine1.text! + "  " + self.txbLine2.text! + "  " + self.removeTrailingSpaces(str: self.txbCity.text!) + ", "
                    } else {
                        newAddressString = self.txbLine1.text! + "  " + self.removeTrailingSpaces(str: self.txbCity.text!) + ", "
                    }
                    
                    let selectedState = self.states[self.pkrState.selectedRow(inComponent: 0)]
                    newAddressString = newAddressString + selectedState + " " + self.txbZip.text!
                    
                    //Assign the new address string
                    self.updateAddress.Address = newAddressString
                    
                    print("After Update:")
                    print(self.updateUser)
                    
                    self.addressStore.save(self.updateAddress, options: nil) { (result: Result<SiteAddress, Swift.Error>) in
                        switch result {
                        case .success(let address):
                            print("Address: \(address)")
                            self.showAlert(title: "Address Updated", message: "Your updates have been successfully saved ...")
                            
                            //Update the default billing and shipping if this address is set for either
                            //Query for all fields using Id.
                            let query = Query(format: "_id == %@", self.selectedID )
                            //Find then update then save.
                            self.dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                                switch result {
                                case .success(let AspNetUsers):
                                    print("AspNetUser: \(AspNetUsers)")
                                    self.updateUser = AspNetUsers[0]
                                    print("Before Update:")
                                    print(self.updateUser)
                                    if self.swDefaultBilling.isOn {
                                        self.updateUser.DefaultBillToAddressId = Int32(self.addressID)!
                                    }
                                    if self.swDefaultShipping.isOn {
                                        self.updateUser.DefaultShipToAddressId = Int32(self.addressID)!
                                    }
                                    print("After Update:")
                                    print(self.updateUser)
                                    
                                    self.dataStore.save(self.updateUser, options: nil) { (result: Result<AspNetUsers, Swift.Error>) in
                                        switch result {
                                        case .success(let user):
                                            print("User: \(user)")
                                            self.tbvAddresses.reloadData()
                                        case .failure(let error):
                                            print("Error: \(error)")
                                        }
                                    }
                                    
                                case .failure(let error):
                                    print("Error: \(error)")
                                }
                            }
                            
                            self.refreshAddressData()
                            self.tbvAddresses.reloadData()
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
            
        }
        
        
    }
    
    @IBAction func btnDelete(_ sender: UIButton) {
        //Delete the selected address
        let alert = UIAlertController(title: "Verify Address Deletion", message: "Are you sure you want to delete this address?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            
            //Check for value of addressID
            if self.addressID != "0" {
                //remove it.
                let query = Query(format: "_id == %@", self.addressID)
                self.addressStore.remove(query, options: nil) { (result: Result<Int, Swift.Error>) in
                    switch result {
                    case .success(let count):
                        print("Count: \(count)")
                        self.refreshAddressData()
                        self.tbvAddresses.reloadData()
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
            } else {
                
                self.showAlert(title: "No Address Selected", message: "Select address to delete first ...")
            }
            
        }))
        
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        //Show the alert
        self.present(alert, animated: true)
        
    }
    
    //Function to remove trailing spaces from a string var
    func removeTrailingSpaces(str: String) -> String {
        
        //Use working var
        var newString: String = str
        
        //Strip off the trailing spaces
        while newString.hasSuffix(" ") {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
    
    @IBAction func btnAddAddress(_ sender: UIButton) {
        //Add the new address
        
        //Put minimum address checks here
        //Must have line1, city, state, and zip
        if self.txbLine1.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include line 1. Please add line 1.")
            return
        }
        
        if self.txbCity.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include a city. Please add a city.")
            return
        }
        
        if self.txbZip.text! == "" {
            self.showAlert(title: "Add Address", message: "Must include a zip code. Please add zip code.")
            return
        }
        
        //Var to hold next address ID value
        var nextAddressId: Int32 = 0
        
        //Var to hold the new address
        let newAddress = SiteAddress()
        
        //This is a temp solution we really want max but here using a sort to get the max value then add one to it.
        let predicate = NSPredicate(format: "_id > %d", 0)
        let sortDescriptor = NSSortDescriptor(key: "_id", ascending: true)
        let querySort = Query(predicate: predicate, sortDescriptors: [sortDescriptor])
        addressStore.find(querySort) { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
            switch result {
            case .success(let SiteAddress):
                
                //This gets the next ID to use.
                print("SiteAddress: \(SiteAddress)")
                self.addresses = SiteAddress
                print(self.addresses[self.addresses.count - 1]._id!)
                nextAddressId = (Int32(self.addresses[self.addresses.count - 1]._id!)! + 1)
                print(nextAddressId)
                
                //Get the rest of the fields and create a new address object then save to addressStore
                //Make sure you add the double space between the address line elements
                newAddress._id = String(nextAddressId)
                newAddress.CustomerId = self.sm.getCustomerId()
                
                //String for full address build.
                var newAddressString: String = ""
                
                //build the new address string
                //Check to see if line2 is provided add if it is
                if self.txbLine2.text! != "" {
                    //line 2 has been provided so include it
                    newAddressString = self.txbLine1.text! + "  " + self.txbLine2.text! + "  " + self.removeTrailingSpaces(str: self.txbCity.text!) + ", "
                } else {
                    newAddressString = self.txbLine1.text! + "  " + self.removeTrailingSpaces(str: self.txbCity.text!) + ", "
                }
                
                let selectedState = self.states[self.pkrState.selectedRow(inComponent: 0)]
                newAddressString = newAddressString + selectedState + " " + self.txbZip.text!
                newAddress.Address = newAddressString
                newAddress.Enabled = 1
                print(newAddress)
                
                self.addressStore.save(newAddress, options: nil) { (result: Result<SiteAddress, Swift.Error>) in
                    switch result {
                    case .success(let address):
                        print("address saved: \(address)")
                        //Reset the address data to only this customers addresses
                        self.refreshAddressData()
                        self.tbvAddresses.reloadData()
                    case .failure(let error):
                        print("Error: \(error)")
                        self.showAlert(title: "Add Address", message: "Error adding address ...")
                    }
                }
            
                case .failure(let error):
                    print("Error: \(error)")
                }
            
            //Update the default billing and shipping if this address is set for either
            //Query for all fields using Id.
            let query = Query(format: "_id == %@", self.selectedID )
            //Find then update then save.
            self.dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                switch result {
                case .success(let AspNetUsers):
                    print("AspNetUser: \(AspNetUsers)")
                    self.updateUser = AspNetUsers[0]
                    print("Before Update:")
                    print(self.updateUser)
                    if self.swDefaultBilling.isOn {
                        self.updateUser.DefaultBillToAddressId = nextAddressId
                    }
                    if self.swDefaultShipping.isOn {
                        self.updateUser.DefaultShipToAddressId = nextAddressId
                    }
                    print("After Update:")
                    print(self.updateUser)
                    
                    self.dataStore.save(self.updateUser, options: nil) { (result: Result<AspNetUsers, Swift.Error>) in
                        switch result {
                        case .success(let user):
                            print("User: \(user)")
                        case .failure(let error):
                            print("Error: \(error)")
                        }
                    }
                    
                case .failure(let error):
                    print("Error: \(error)")
                }
            }
        
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        //Get the selected customerID
        selectedID = sm.getCustomerId()
        
        //Show selected ID
        print("SelectedID = \(selectedID)")
        
        //Query for all addresses using Id.
        let queryAddress = Query(format: "CustomerId == %@", selectedID )
        addressStore.find(queryAddress) { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
            switch result {
            case .success(let SiteAddress):
                print("SiteAddress: \(SiteAddress)")
                self.addresses = SiteAddress
                self.tbvAddresses.reloadData()
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        //Query for all fields using Id.
        let query = Query(format: "_id == %@", self.selectedID )
        self.dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
            switch result {
            case .success(let AspNetUsers):
                print("AspNetUser: \(AspNetUsers)")
                self.user = AspNetUsers
                let defaultBillingId = self.user[0].DefaultBillToAddressId
                let defaultShippingID = self.user[0].DefaultShipToAddressId
                
                //If this address is set to default billing then turn on switch
                if defaultBillingId.description == self.addressID {
                    self.swDefaultBilling.isOn = true
                } else {
                    self.swDefaultBilling.isOn = false
                }
                
                //If this address is set to default shipping then turn on switch
                if defaultShippingID.description == self.addressID {
                    self.swDefaultShipping.isOn = true
                } else {
                    self.swDefaultShipping.isOn = false
                }
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        self.lblStatusMessage.text = "Select address to update below or New to add ..."
    }
    
    func refreshAddressData() {
        
        //Get the selected customerID
        selectedID = sm.getCustomerId()
        
        //Show selected ID
        print("SelectedID = \(selectedID)")
        
        //Query for all addresses using Id.
        let queryAddress = Query(format: "CustomerId == %@", selectedID )
        addressStore.find(queryAddress) { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
            switch result {
            case .success(let SiteAddress):
                print("SiteAddress: \(SiteAddress)")
                self.addresses = SiteAddress
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        self.lblStatusMessage.text = "Select address to update below or New to add ..."
        
        //Query for all fields using Id.
        let query = Query(format: "_id == %@", selectedID )
        dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
            switch result {
            case .success(let AspNetUsers):
                print("AspNetUser: \(AspNetUsers)")
                self.user = AspNetUsers
                let defaultBillingId = self.user[0].DefaultBillToAddressId
                let defaultShippingID = self.user[0].DefaultShipToAddressId
                
                //If this address is set to default billing then turn on switch
                if defaultBillingId.description == self.addressID {
                    self.swDefaultBilling.isOn = true
                } else {
                    self.swDefaultBilling.isOn = false
                }
                
                //If this address is set to default shipping then turn on switch
                if defaultShippingID.description == self.addressID {
                    self.swDefaultShipping.isOn = true
                } else {
                    self.swDefaultShipping.isOn = false
                }
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //parse the address string and place values in the form.
        
        //Need to determine if this is a two line address or three line address
        //If only one comma exists then its two lines not three
        //check for number of commas in the address string
        
        //First clear all values that can be entered
        self.txbLine1.text = ""
        self.txbLine2.text = ""
        self.txbCity.text = ""
        self.txbZip.text = ""
        
        //Get the address ID to lookup for default billing and shipping
        self.addressID = addresses[indexPath.row]._id!
        
        //Get selected address to parse
        let selectedAddress = addresses[indexPath.row].Address!
        //let addressNumberOfLines: Int = selectedAddress.components(separatedBy: ",").count
        let addressLines = selectedAddress.components(separatedBy: "  ")
        
        //first see how many sections we got from the split
        if addressLines.count == 1 {
            
        }
        
        if addressLines.count == 2 {
            self.txbLine1.text = addressLines[0]
            let lastLine: String = addressLines[1]
            
            //Get Zip from last line
            let indexEnd = lastLine.index(lastLine.endIndex, offsetBy: -5)
            let ZipString = lastLine[indexEnd...]
            self.txbZip.text = ZipString.description
            
            //Parse of the city from last line of address
            let index = lastLine.index(of: ",")!
            let newStr = lastLine[..<index]
            self.txbCity.text = newStr.description
            
            //State code starts at first non space after comma and goes for two characters
            let lastLineParts = lastLine.components(separatedBy: " ")
            var selectedStateCode: String = ""
            
            //One word city
            if lastLineParts.count == 3 {
                selectedStateCode = lastLineParts[1]
            }
            //Two word city with space
            if lastLineParts.count == 4 {
                selectedStateCode = lastLineParts[2]
            }
            //Three word cities
            if lastLineParts.count == 5 {
                selectedStateCode = lastLineParts[3]
            }
            
            //Get the index of the selected state value to set the picker selected value
            let indexToUse = self.getIndexOfStateValue(checkState: selectedStateCode)
            self.pkrState.selectRow(indexToUse, inComponent: 0, animated: true)
            
        }
        
        if addressLines.count == 3 {
            //Three elements to parse
            self.txbLine1.text = addressLines[0]
            self.txbLine2.text = addressLines[1]
            let lastLine: String = addressLines[2]
            
            //Get Zip from last line
            let indexEnd = lastLine.index(lastLine.endIndex, offsetBy: -5)
            let ZipString = lastLine[indexEnd...]
            self.txbZip.text = ZipString.description
            
            //Parse of the city from last line of address
            let index = lastLine.index(of: ",")!
            let newStr = lastLine[..<index]
            self.txbCity.text = newStr.description
            
            //State code starts at first non space after comma and goes for two characters
            let lastLineParts = lastLine.components(separatedBy: " ")
            var selectedStateCode: String = ""
            
            //One word city
            if lastLineParts.count == 3 {
                selectedStateCode = lastLineParts[1]
            }
            //Two word city with space
            if lastLineParts.count == 4 {
                selectedStateCode = lastLineParts[2]
            }
            //Three word cities
            if lastLineParts.count == 5 {
                selectedStateCode = lastLineParts[3]
            }
            
            //Get the index of the selected state value to set the picker selected value
            let indexToUse = self.getIndexOfStateValue(checkState: selectedStateCode)
            self.pkrState.selectRow(indexToUse, inComponent: 0, animated: true)
            
        }
        
        if addressLines.count == 4 {
            //Three elements to parse
            self.txbLine1.text = addressLines[0] + " " + addressLines[1]
            self.txbLine2.text = addressLines[2]
            let lastLine: String = addressLines[3]
            
            //Get Zip from last line
            let indexEnd = lastLine.index(lastLine.endIndex, offsetBy: -5)
            let ZipString = lastLine[indexEnd...]
            self.txbZip.text = ZipString.description
            
            //Parse of the city from last line of address
            let index = lastLine.index(of: ",")!
            let newStr = lastLine[..<index]
            self.txbCity.text = newStr.description
            
            //State code starts at first non space after comma and goes for two characters
            let lastLineParts = lastLine.components(separatedBy: " ")
            var selectedStateCode: String = ""
            
            //One word city
            if lastLineParts.count == 3 {
                selectedStateCode = lastLineParts[1]
            }
            //Two word city with space
            if lastLineParts.count == 4 {
                selectedStateCode = lastLineParts[2]
            }
            //Three word cities
            if lastLineParts.count == 5 {
                selectedStateCode = lastLineParts[3]
            }
            
            //Get the index of the selected state value to set the picker selected value
            let indexToUse = self.getIndexOfStateValue(checkState: selectedStateCode)
            self.pkrState.selectRow(indexToUse, inComponent: 0, animated: true)
            
        }
        
        //This should never happen.
        if addressLines.count == 5 {
            
        }
        
        //Query for all fields using Id.
        let query = Query(format: "_id == %@", self.selectedID )
        self.dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
            switch result {
            case .success(let AspNetUsers):
                print("AspNetUser: \(AspNetUsers)")
                self.user = AspNetUsers
                let defaultBillingId = self.user[0].DefaultBillToAddressId
                let defaultShippingID = self.user[0].DefaultShipToAddressId
                
                //If this address is set to default billing then turn on switch
                if defaultBillingId.description == self.addressID {
                    self.swDefaultBilling.isOn = true
                } else {
                    self.swDefaultBilling.isOn = false
                }
                
                //If this address is set to default shipping then turn on switch
                if defaultShippingID.description == self.addressID {
                    self.swDefaultShipping.isOn = true
                } else {
                    self.swDefaultShipping.isOn = false
                }
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }
    
    //Get the index for the value desired
    func getIndexOfStateValue(checkState: String) -> Int {
        //Loop through the values and return the correct index
        var returnIndex: Int = 0
        for (index,_) in states.enumerated() {
            if states[index] == checkState {
                returnIndex = index
                break
            }
        }
        
        return returnIndex
    }
    
    
}
