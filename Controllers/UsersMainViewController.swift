//
//  UsersMainViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/11/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class UsersMainViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UITextFieldDelegate,UITabBarDelegate {
    
    //Create the Datastore for the AspNetUsers collection
    let dataStore = DataStore<AspNetUsers>.collection()
    
    //Create the Datastore for the addresses
    let addressStore = DataStore<SiteAddress>.collection()
    
    //Create the Datastore for the physicians
    let physicianStore = DataStore<Physician>.collection()
    
    //Var to hold return from find in Kinvey - temp
    var users = AnyRandomAccessCollection<AspNetUsers>([])
    
    //Var to hold search result with filtered records
    var usersFiltered = AnyRandomAccessCollection<AspNetUsers>([])
    
    //Var to hold return from find for addresses
    var addresses = AnyRandomAccessCollection<SiteAddress>([])
    
    //Var to hold return from find for addresses
    var physicians = AnyRandomAccessCollection<Physician>([])
    
    //Add the sharing manager
    let sm = SharingManager.sharedInstance
    
    //Search active var
    var searchActive: Bool = false
    

    @IBOutlet weak var srcUserListing: UISearchBar!
    @IBOutlet weak var tvwUsersListing: UITableView!
    
    @IBOutlet weak var tbiLogin: UITabBarItem!
    @IBOutlet weak var tbiNewUser: UITabBarItem!
    @IBOutlet weak var utbMain: UITabBar!
    
    
    //Function to show alerts to user
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var numRows: Int = 1
        
        if searchActive {
            numRows = Int(self.usersFiltered.count)
        } else {
            numRows = Int(self.users.count)
        }
        
        return numRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let userCell = tvwUsersListing.dequeueReusableCell(withIdentifier: "UserListingCell", for: indexPath) as! UserListingTVCell
        
        if searchActive && indexPath.row < Int(self.usersFiltered.count) {
            //Set cell data
            userCell.txbContactName.text = self.usersFiltered[indexPath.row].ContactName as String!
            userCell.txbCustomerNumber.text = self.usersFiltered[indexPath.row].CustomerNumber as String!
            userCell.txbFacilityName.text = self.usersFiltered[indexPath.row].FacilityName as String!
        } else {
            //Set cell data
            userCell.txbContactName.text = self.users[indexPath.row].ContactName as String!
            userCell.txbCustomerNumber.text = self.users[indexPath.row].CustomerNumber as String!
            userCell.txbFacilityName.text = self.users[indexPath.row].FacilityName as String!
        }
        
        
        return userCell
    }
    
    func refreshData() {
        //Get all users
        dataStore.find() { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
            switch result {
            case .success(let AspNetUsers):
                print("AspNetUsers: \(AspNetUsers)")
                self.users = AspNetUsers
                self.tvwUsersListing.reloadData()
                print(self.users.count)
            case .failure(let error):
                print("Error: \(error)")
            }
        }
       
        //Get all addresses on refresh
        addressStore.find() { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
            switch result {
            case .success(let SiteAddress):
                print("SiteAddress: \(SiteAddress)")
                self.addresses = SiteAddress
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        //Query for all physicians on refresh
        physicianStore.find() { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physicians):
                print("Physicians: \(physicians)")
                self.physicians = physicians
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.refreshData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //do the segue prep here and then show the details
        
        print(self.users[indexPath.row]._id!)
        print(self.users[indexPath.row].CustomerNumber!)
        print(self.users[indexPath.row].ContactName!)
        
        
        //Store the selected ID for next form
        if searchActive {
            sm.setCustomerId(custID: self.usersFiltered[indexPath.row]._id!)
        } else {
           sm.setCustomerId(custID: self.users[indexPath.row]._id!)
        }
        
        performSegue(withIdentifier: "ShowUserDetails", sender: self)
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if Int(self.usersFiltered.count)>0 {
            searchActive = true
        } else {
            searchActive = false
        }
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       
        if srcUserListing.text == "" {
            searchActive = false
        } else {
            
            if (srcUserListing.text?.lengthOfBytes(using: String.Encoding.ascii))!>0 {
                
                //Query for all fields using Id.
                let query = Query(format: "CustomerNumber == %@", srcUserListing.text! )
                dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
                    switch result {
                    case .success(let AspNetUsers):
                        print("AspNetUser: \(AspNetUsers)")
                        self.usersFiltered = AspNetUsers
                        self.tvwUsersListing.reloadData()
                    case .failure(let error):
                        print("Error: \(error)")
                    }
                }
                
            }
        }
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            //Nothing to search user tapped X
            
            searchActive = false
            self.tvwUsersListing.reloadData()
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //Add fields to resign first responder here
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        //Dismiss keyboard
        self.view.endEditing(true)
    }
    
    //When user clicks on a tab bar item.
    func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem)
    {
        print("selected item was: \(item.title!)")
        
        switch item.title! {
        case "Login":
            print("Login")
            self.performSegue(withIdentifier: "ShowLogin", sender: self)
        case "New User":
            print("New User")
            self.performSegue(withIdentifier: "ShowAddUser", sender: self)
        default :
            print("Default")
        }
        
    }
   
}
