//
//  UserDetailViewController.swift
//  ProductivityAdmin
//
//  Created by localadmin on 1/11/18.
//  Copyright © 2018 BostonScientific. All rights reserved.
//

import UIKit
import Kinvey

class UserDetailViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBAction func btnEditUser(_ sender: UIBarButtonItem) {
        
        //set selected user in share manager
        //Perform seque
        self.performSegue(withIdentifier: "EditUserDetails", sender: self)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Check tag for which tableview is being serviced tag=0 for Addresses and tag=1 for Physicians
        
        if tableView.tag == 0 {
            return Int(self.addresses.count)
        }
        
        if tableView.tag == 1 {
            return Int(self.physicians.count)
        }
       
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Check tag for which tableview is being serviced tag=0 for Addresses and tag=1 for Physicians
        
        var cell: UITableViewCell? = nil
        
        if tableView.tag == 0 {
           let addressCell = tbvAddress.dequeueReusableCell(withIdentifier: "AddressCell", for: indexPath) as! AddressTVCell
            //Get the cell data from Kinvey object and set the cell textbox
           addressCell.txbAddress.text = addresses[indexPath.row].Address!
        
           cell = addressCell
        }
        
        if tableView.tag == 1 {
            let physicianCell = tbvPhysicians.dequeueReusableCell(withIdentifier: "PhysicianCell", for: indexPath) as! PhysicianTVCell
            
            
            if physicians[indexPath.row].FirstName == nil {
                physicianCell.txbPhysician.text = "Unknown-FN" + " " + physicians[indexPath.row].LastName!
            }
            
            if physicians[indexPath.row].LastName == nil {
                physicianCell.txbPhysician.text = physicians[indexPath.row].FirstName! + " " + "Unknown-LN"
            }
            
            if physicians[indexPath.row].FirstName != nil && physicians[indexPath.row].LastName != nil {
                physicianCell.txbPhysician.text = physicians[indexPath.row].FirstName! + " " + physicians[indexPath.row].LastName!
            }
            
            
            cell = physicianCell
        }
        
        return cell!
    }
    
    
    
    @IBOutlet weak var lblCustomerNumber: UILabel!
    @IBOutlet weak var lblContactName: UILabel!
    @IBOutlet weak var lblFacilityName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblPhoneNumber: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblRepEmail: UILabel!
    @IBOutlet weak var lblDeviceID: UILabel!
    
    @IBOutlet weak var tbvAddress: UITableView!
    
    @IBOutlet weak var tbvPhysicians: UITableView!
    
    //Add the sharing manager
    let sm = SharingManager.sharedInstance
    
    //Var to hold return from find for users
    var user = AnyRandomAccessCollection<AspNetUsers>([])
    
    //Var to hold return from find for addresses
    var addresses = AnyRandomAccessCollection<SiteAddress>([])
    
    //Var to hold return from find for addresses
    var physicians = AnyRandomAccessCollection<Physician>([])
    
    //Create the Datastore for the users
    let dataStore = DataStore<AspNetUsers>.collection()
    
    //Create the Datastore for the addresses
    let addressStore = DataStore<SiteAddress>.collection()
    
    //Create the Datastore for the physicians
    let physicianStore = DataStore<Physician>.collection()
    
    var selectedID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Get the selected customerID
        selectedID = sm.getCustomerId()
        
        //Show selected ID
        print("SelectedID = \(selectedID)")
        
        //Query for all fields using Id.
        let query = Query(format: "_id == %@", selectedID )
        dataStore.find(query) { (result: Result<AnyRandomAccessCollection<AspNetUsers>, Swift.Error>) in
            switch result {
            case .success(let AspNetUsers):
                print("AspNetUser: \(AspNetUsers)")
                self.user = AspNetUsers
                self.lblCustomerNumber.text = self.user[0].CustomerNumber!
                self.lblContactName.text = self.user[0].ContactName!
                self.lblFacilityName.text = self.user[0].FacilityName!
                self.lblUsername.text = self.user[0].UserName!
                self.lblPhoneNumber.text = self.user[0].PhoneNumber!
                self.lblEmail.text = self.user[0].Email!
                self.lblRepEmail.text = self.user[0].RepEmail!
                
                //Test for nil
                if self.user[0].DeviceId != nil {
                     self.lblDeviceID.text = self.user[0].DeviceId!
                } else {
                      self.lblDeviceID.text = ""
                }
                
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        //Query for all fields using Id.
        let queryAddress = Query(format: "CustomerId == %@", selectedID )
        addressStore.find(queryAddress) { (result: Result<AnyRandomAccessCollection<SiteAddress>, Swift.Error>) in
            switch result {
            case .success(let SiteAddress):
                print("SiteAddress: \(SiteAddress)")
                self.addresses = SiteAddress
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
        //Query for all fields using Id.
        let queryPhysician = Query(format: "CustomerId == %@", selectedID )
        physicianStore.find(queryPhysician) { (result: Result<AnyRandomAccessCollection<Physician>, Swift.Error>) in
            switch result {
            case .success(let physicians):
                print("Physicians: \(physicians)")
                self.physicians = physicians
            case .failure(let error):
                print("Error: \(error)")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
